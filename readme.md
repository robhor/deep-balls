

## Pipeline

### Generate data

 ```shell
 python generator_glfw.py
 ```

### Transform for neural network

 ```shell
 python transformer.py
 ```

### Train the neural network

 ```shell
 python model.py \
  --mode train \
  --output_dir out/pix2pix/train \
  --input_dir out/transformer/train
 ```

### Test the model

 ```shell
 python model.py \
  --mode test \
  --output_dir out/pix2pix/test \
  --input_dir out/transformer/test \
  --checkpoint out/pix2pix/train
 ```

### Interactive demo

Move camera around with the mouse and WASD, right mouse button to generate new balls, middle mouse button or number keys to switch rendering mode

 ```shell
 python demo.py --checkpoint out/pix2pix/train --output_dir out/demo
 ```

## Training data and trained model

The data the model was trained on, as well as the trained model, is found in [data.zip](data.zip).