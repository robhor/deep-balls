import cv2
import numpy as np
from gevent import os

IMAGE_WIDTH = 512
IMAGE_HEIGHT = 512

dirname = os.path.dirname(__file__)
data_dir = os.path.join(dirname, "out/demo/")
examples = os.listdir(data_dir)
examples = list(sorted(examples))
examples = map(lambda x: os.path.join(data_dir, x), examples)

black = np.full((IMAGE_WIDTH, IMAGE_HEIGHT), 0)

def read_image(path):
    print(path)
    img = cv2.imread(path, cv2.IMREAD_COLOR)
    return img

def create_comparison(path):
    ai_norm = read_image(os.path.join(path, 'ai_norm.png'))
    ai_depth = read_image(os.path.join(path, 'ai_depth.png'))
    ai_color = read_image(os.path.join(path, 'ai_color.png'))
    obj_norm = read_image(os.path.join(path, 'obj_norm.png'))
    obj_depth = read_image(os.path.join(path, 'obj_depth.png'))
    obj_color = read_image(os.path.join(path, 'obj_color.png'))

    retval, ai_outline = cv2.threshold(ai_norm[:,:,0], 0, 255, cv2.THRESH_BINARY)
    retval, obj_outline = cv2.threshold(obj_norm[:, :, 0], 0, 255, cv2.THRESH_BINARY)

    comparison_factor = 1
    comparison_outline = np.stack((black, ai_outline, obj_outline), 2) * comparison_factor
    comparison_norm = cv2.absdiff(ai_norm, obj_norm) * comparison_factor
    comparison_depth = cv2.absdiff(ai_depth, obj_depth) * comparison_factor
    comparison_color = cv2.absdiff(ai_color, obj_color) * comparison_factor

    cv2.imwrite(os.path.join(path, 'comparison_outline.png'), comparison_outline)
    cv2.imwrite(os.path.join(path, 'comparison_norm.png'), comparison_norm)
    cv2.imwrite(os.path.join(path, 'comparison_depth.png'), comparison_depth)
    cv2.imwrite(os.path.join(path, 'comparison_color.png'), comparison_color)


def create_comparisons(examples):
    for in_idx, dir in enumerate(examples):
        if os.path.isdir(dir):
            create_comparison(dir)


create_comparisons(examples)