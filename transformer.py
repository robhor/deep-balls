import cv2
import numpy as np
from gevent import os

IMAGE_WIDTH = 512
IMAGE_HEIGHT = 512

dirname = os.path.dirname(__file__)
data_dir = os.path.join(dirname, "out/generator/")
examples = os.listdir(data_dir)
examples = list(sorted(examples))

train_size = int(len(examples))

train_data = examples
test_data = [] #examples[train_size:]

white = np.full((IMAGE_WIDTH, IMAGE_HEIGHT), 255)

def read_image(path):
    img = cv2.imread(path, cv2.IMREAD_COLOR)
    img = cv2.resize(img, (IMAGE_WIDTH, IMAGE_HEIGHT), interpolation=cv2.INTER_CUBIC)
    return img


def create_ABs(examples, output_dir):
    for in_idx, dir in enumerate(examples):
        circle_depth_path = os.path.join(data_dir, dir, "circle_depth.png")
        circle_color_path = os.path.join(data_dir, dir, "circle_color.png")
        obj_depth_path = os.path.join(data_dir, dir, "obj_depth.png")
        obj_color_path = os.path.join(data_dir, dir, "obj_color.png")
        obj_normals_path = os.path.join(data_dir, dir, "obj_norm.png")

        if not os.path.isfile(circle_depth_path) or not os.path.isfile(obj_depth_path):
            continue

        circle_depth = read_image(circle_depth_path)[:,:,0]
        circle_color = read_image(circle_color_path)
        obj_depth = read_image(obj_depth_path)[:,:,0]
        obj_color = read_image(obj_color_path)
        obj_normals = read_image(obj_normals_path)

        rgb = np.concatenate((circle_color, obj_color, obj_normals), axis=1)
        alpha = np.concatenate((circle_depth, obj_depth, white), axis=1)
        alpha = np.expand_dims(alpha, 2)

        img = np.concatenate((rgb, alpha), axis=2)

        out_path = os.path.join(output_dir, dir + '.png')
        print(out_path)
        cv2.imwrite(out_path, img)


train_dir = os.path.join(dirname, "out/transformer/train")
test_dir = os.path.join(dirname, "out/transformer/test")
os.makedirs(train_dir, exist_ok=True)
os.makedirs(test_dir, exist_ok=True)

create_ABs(train_data, train_dir)
create_ABs(test_data, test_dir)
