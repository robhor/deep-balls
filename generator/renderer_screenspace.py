import os

from OpenGL.GL import *


class CircleMetaballsRenderer:
    billboardProgram = -1
    circleColorUniform = -1
    circleRadiusUniform = -1
    circleOutputTypeUniform = -1

    def setup(self):
        dirname = os.path.dirname(__file__)
        vert_file = open(os.path.join(dirname, 'circle.vs'))
        vert = vert_file.read()
        vert_file.close()

        vs_id = glCreateShader(GL_VERTEX_SHADER)
        glShaderSource(vs_id, vert)
        glCompileShader(vs_id)
        if not glGetShaderiv(vs_id, GL_COMPILE_STATUS):
            raise Exception('failed to compile shader "%s":\n%s' % ("vert", glGetShaderInfoLog(vs_id).decode()))

        frag_file = open(os.path.join(dirname, 'circle.fs'))
        frag = frag_file.read()
        frag_file.close()

        fs_id = glCreateShader(GL_FRAGMENT_SHADER)
        glShaderSource(fs_id, frag)
        glCompileShader(fs_id)
        if not glGetShaderiv(fs_id, GL_COMPILE_STATUS):
            raise Exception('failed to compile shader "%s":\n%s' % ("frag", glGetShaderInfoLog(fs_id).decode()))

        self.billboardProgram = glCreateProgram()
        glAttachShader(self.billboardProgram, vs_id)
        glAttachShader(self.billboardProgram, fs_id)
        glLinkProgram(self.billboardProgram)

        if not glGetProgramiv(self.billboardProgram, GL_LINK_STATUS):
            raise Exception("Link failed")

        glUseProgram(self.billboardProgram)

        self.circleColorUniform = glGetUniformLocation(self.billboardProgram, "color")
        self.circleRadiusUniform = glGetUniformLocation(self.billboardProgram, "radius")
        self.circleOutputTypeUniform = glGetUniformLocation(self.billboardProgram, "outputType")

    def __draw_billboard(self, balls, index):
        ball = balls['balls'][index]
        s = 3 * ball[3]
        vertices = [
            - s / 2, - s / 2, 0.0,
            + s / 2, - s / 2, 0.0,
            + s / 2, + s / 2, 0.0,
            - s / 2, + s / 2, 0.0
        ]
        texcoords = [
            0, 0, 0,
            1, 0, 0,
            1, 1, 0,
            0, 1, 0
        ]
        indices = [
            0, 1, 2, 2, 3, 0
        ]

        glUniform3f(self.circleColorUniform, *balls['colors'][index])
        glUniform1f(self.circleRadiusUniform, ball[3])

        glEnable(GL_TEXTURE_2D)
        glBegin(GL_TRIANGLES)
        glNormal3f(ball[0], ball[1], ball[2])
        for i in range(0, len(indices), 3):
            index = indices[i] * 3
            glTexCoord2f(*texcoords[index:index + 2])
            glVertex3f(*vertices[index:index + 3])

            index = indices[i + 1] * 3
            glTexCoord2f(*texcoords[index:index + 2])
            glVertex3f(*vertices[index:index + 3])

            index = indices[i + 2] * 3
            glTexCoord2f(*texcoords[index:index + 2])
            glVertex3f(*vertices[index:index + 3])

        glEnd()

    def __render_with_type(self, render_type, balls):
        glUseProgram(self.billboardProgram)

        glEnable(GL_DEPTH_TEST)
        glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT)
        glDepthFunc(GL_LESS)
        glUniform1i(self.circleOutputTypeUniform, render_type)
        for x in range(0, balls['count']):
            self.__draw_billboard(balls, x)

    def render_color(self, balls):
        glClearColor(0, 0, 0, 1)
        self.__render_with_type(0, balls)

    def render_depth(self, balls):
        glClearColor(1, 1, 1, 1)
        self.__render_with_type(1, balls)
