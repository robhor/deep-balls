import os

from OpenGL.GL import *


class ObjectMetaballsRenderer:
    __objectballsProgram = -1
    __metaballsUniform = -1
    __metaballColorsUniform = -1
    __ballsCountUniform = -1
    __outputTypeUniform = -1

    def setup(self):
        vert = """
            attribute vec4 v_position;
            varying vec2 uv;

            void main() {
              gl_Position = v_position;
              uv = (v_position.xy + 1.) * 0.5;
            }
            """

        vsId = glCreateShader(GL_VERTEX_SHADER)
        glShaderSource(vsId, vert)
        glCompileShader(vsId)
        if not glGetShaderiv(vsId, GL_COMPILE_STATUS):
            raise Exception('failed to compile shader "%s":\n%s' % ("vert", glGetShaderInfoLog(vsId).decode()))

        dirname = os.path.dirname(__file__)
        frag_file = open(os.path.join(dirname, 'objectballs.fs'))
        frag = frag_file.read()
        frag_file.close()

        fsId = glCreateShader(GL_FRAGMENT_SHADER)
        glShaderSource(fsId, frag)
        glCompileShader(fsId)
        if not glGetShaderiv(fsId, GL_COMPILE_STATUS):
            raise Exception('failed to compile shader "%s":\n%s' % ("frag", glGetShaderInfoLog(fsId).decode()))

        self.__objectballsProgram = glCreateProgram()
        glAttachShader(self.__objectballsProgram, vsId)
        glAttachShader(self.__objectballsProgram, fsId)
        glLinkProgram(self.__objectballsProgram)

        if not glGetProgramiv(self.__objectballsProgram, GL_LINK_STATUS):
            raise Exception("Link failed")

        glUseProgram(self.__objectballsProgram)

        self.__metaballsUniform = glGetUniformLocation(self.__objectballsProgram, "metaballs")
        self.__metaballColorsUniform = glGetUniformLocation(self.__objectballsProgram, "metaballsColor")
        self.__ballsCountUniform = glGetUniformLocation(self.__objectballsProgram, "ballsCount")
        self.__outputTypeUniform = glGetUniformLocation(self.__objectballsProgram, "outputType")

    def __draw_fullscreen_quad(self):
        vertices = [
            -1, -1, 0,
            1, -1, 0,
            1, 1, 0,
            -1, 1, 0
        ]
        indices = [
            0, 1, 2, 2, 3, 0
        ]

        glBegin(GL_TRIANGLES)
        for i in range(0, len(indices), 3):
            index = indices[i] * 3
            glVertex3f(*vertices[index:index + 3])

            index = indices[i + 1] * 3
            glVertex3f(*vertices[index:index + 3])

            index = indices[i + 2] * 3
            glVertex3f(*vertices[index:index + 3])

        glEnd()

    def __render_with_type(self, render_type, balls):
        glUseProgram(self.__objectballsProgram)

        glDisable(GL_DEPTH_TEST)
        glClearColor(0, 0, 0, 1)
        glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT)
        glUniform1i(self.__outputTypeUniform, render_type)

        glUniform4fv(self.__metaballsUniform, balls['count'], balls['balls'])
        glUniform3fv(self.__metaballColorsUniform, balls['count'], balls['colors'])
        glUniform1i(self.__ballsCountUniform, balls['count'])

        self.__draw_fullscreen_quad()

    def render_color(self, balls):
        self.__render_with_type(0, balls)

    def render_depth(self, balls):
        self.__render_with_type(1, balls)

    def render_normals(self, balls):
        self.__render_with_type(2, balls)

    def render_shaded(self, balls):
        self.__render_with_type(3, balls)
