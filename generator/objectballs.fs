#version 120

#define METABALLS_COUNT 45

uniform vec4 metaballs[METABALLS_COUNT];
uniform vec3 metaballsColor[METABALLS_COUNT];
uniform int ballsCount;
uniform int outputType;

varying vec2 uv;

#define FOV 30.
#define MIN_DEPTH 7.
#define MAX_DEPTH 25.
#define DEPTH_STEP 0.01
#define THRESHOLD 1.0

float ff(vec4 metaball, vec3 pos) {
    float dist = length(pos - metaball.xyz);
    return 1 / pow(dist / metaball.w, 2.);
}

vec3 ffn(vec4 metaball, vec3 pos) {
    float a = metaball.x;
    float b = metaball.y;
    float c = metaball.z;
    float x = pos.x;
    float y = pos.y;
    float z = pos.z;
    return vec3(2*(a-x), 2*(b-y), 2*(c-z)) / pow(pow(x - a, 2) + pow(y - b, 2) + pow(z - c, 2), 2);
}

vec3 ray(vec2 uv, float dist) {
    float camPlaneDist = 1.0;
    float fov = radians(FOV);
    vec2 centeredUv = (uv - 0.5) * 2;
    vec3 dir = normalize(vec3(centeredUv * camPlaneDist * tan(fov), camPlaneDist));
	vec3 pos = dist * dir;
    return pos;
}

vec4 depthRay(vec2 uv) {
    for (float depth = MIN_DEPTH; depth < MAX_DEPTH; depth += DEPTH_STEP) {
        vec3 pos = ray(uv, depth);
        float fieldValue = 0.;
        vec3 color = vec3(0.);

        for (int i = 0; i < ballsCount; i++) {
            float dist = length(pos - metaballs[i].xyz);
            float ballField = ff(metaballs[i], pos);
            fieldValue += ballField;

            color += metaballsColor[i]*ballField;
        }

        if (fieldValue > THRESHOLD) {
            vec3 normColor = color / fieldValue;
            return vec4(normColor, depth);
            break;
        }
    }

	return vec4(0., 0., 0., 1000.);
}

vec3 normRay(vec2 uv) {
    for (float depth = MIN_DEPTH; depth < MAX_DEPTH; depth += DEPTH_STEP) {
        vec3 pos = ray(uv, depth);
        float fieldValue = 0.;
        vec3 norm = vec3(0.);

        for (int i = 0; i < ballsCount; i++) {
            float dist = length(pos - metaballs[i].xyz);
            float ballField = ff(metaballs[i], pos);
            fieldValue += ballField;

            norm += ffn(metaballs[i], pos); // / metaballs[i].w;
        }

        if (fieldValue > THRESHOLD) {
            return normalize(norm);
        }
    }

	return vec3(0., 0., 0.);
}

float blinnPhongSpecular(
    vec3 lightDirection,
    vec3 viewDirection,
    vec3 surfaceNormal,
    float shininess) {

    vec3 H = normalize(viewDirection + lightDirection);
    return pow(max(0.0, dot(surfaceNormal, H)), shininess);
}

void mainImage( out vec4 fragColor, in vec2 fragCoord)
{
    // Output to screen
    fragColor = vec4(0.0, 0.0, 0.0, 1.0);

    if (outputType == 0) {
        // color
        vec4 depthRayv = depthRay(uv);
        fragColor.rgb = depthRayv.rgb;
    } else if (outputType == 1) {
        // depth
        vec4 depthRayv = depthRay(uv);
        fragColor.rgb = vec3((depthRayv.w - MIN_DEPTH) / (MAX_DEPTH - MIN_DEPTH));
    } else if (outputType == 2) {
        // normals
        vec3 norm = normRay(uv);
        fragColor.rgb = (norm + 1.) * 0.5 * length(norm);
    } else if (outputType == 3) {
        // simple blinn phong shading
        vec3 lightPosition = vec3(3., 0., 0.);

        vec4 depthRayv = depthRay(uv);
        vec3 surfaceNormal = normRay(uv);

        vec3 surfacePosition = ray(uv, depthRayv.w);
        vec3 eyeDirection = normalize(-surfacePosition);
        vec3 lightDirection = normalize(lightPosition - surfacePosition);
        vec3 normal = normalize(surfaceNormal);

        float shininess = 10.;
        float power = blinnPhongSpecular(lightDirection, eyeDirection, -normal, shininess);
        float diff = dot(lightDirection, normal);

        fragColor.rgb = depthRayv.rgb * max(diff, 0.5) + vec3(power);
    }
}

void main() {
    mainImage(gl_FragColor, uv);
}
