import sys
from OpenGL.GL import *
from OpenGL.GLUT import *
from PIL import Image
import random

name = b'ball_glut'
size = (1024, 1024)
objectballsProgram = -1
metaballsUniform = -1
metaballColorsUniform = -1
ballsCountUniform = -1
outputTypeUniform = -1

billboardProgram = -1
circleColorUniform = -1
circleRadiusUniform = -1
circleOutputTypeUniform = -1


def setupObjectballsShader():
    global objectballsProgram
    vert = """
        attribute vec4 v_position;
        varying vec2 uv;

        void main() {
          gl_Position = v_position;
          uv = (v_position + 1.) * 0.5;
        }
        """

    vsId = glCreateShader(GL_VERTEX_SHADER)
    glShaderSource(vsId, vert)
    glCompileShader(vsId)
    if not glGetShaderiv(vsId, GL_COMPILE_STATUS):
        raise Exception('failed to compile shader "%s":\n%s' % ("vert", glGetShaderInfoLog(vsId).decode()))

    fragFile = open('objectballs.fs')
    frag = fragFile.read()
    fragFile.close()

    fsId = glCreateShader(GL_FRAGMENT_SHADER)
    glShaderSource(fsId, frag)
    glCompileShader(fsId)
    if not glGetShaderiv(fsId, GL_COMPILE_STATUS):
        raise Exception('failed to compile shader "%s":\n%s' % ("frag", glGetShaderInfoLog(fsId).decode()))

    objectballsProgram = glCreateProgram()
    glAttachShader(objectballsProgram, vsId)
    glAttachShader(objectballsProgram, fsId)
    glLinkProgram(objectballsProgram)

    if not glGetProgramiv(objectballsProgram, GL_LINK_STATUS):
        raise Exception("Link failed")

    glUseProgram(objectballsProgram)

    global metaballsUniform
    global metaballColorsUniform
    global ballsCountUniform
    global outputTypeUniform
    metaballsUniform = glGetUniformLocation(objectballsProgram, "metaballs")
    metaballColorsUniform = glGetUniformLocation(objectballsProgram, "metaballsColor")
    ballsCountUniform = glGetUniformLocation(objectballsProgram, "ballsCount")
    outputTypeUniform = glGetUniformLocation(objectballsProgram, "outputType")


def setupBillboardsShader():
    global billboardProgram
    vertFile = open('circle.vs')
    vert = vertFile.read()
    vertFile.close()

    vsId = glCreateShader(GL_VERTEX_SHADER)
    glShaderSource(vsId, vert)
    glCompileShader(vsId)
    if not glGetShaderiv(vsId, GL_COMPILE_STATUS):
        raise Exception('failed to compile shader "%s":\n%s' % ("vert", glGetShaderInfoLog(vsId).decode()))

    fragFile = open('circle.fs')
    frag = fragFile.read()
    fragFile.close()

    fsId = glCreateShader(GL_FRAGMENT_SHADER)
    glShaderSource(fsId, frag)
    glCompileShader(fsId)
    if not glGetShaderiv(fsId, GL_COMPILE_STATUS):
        raise Exception('failed to compile shader "%s":\n%s' % ("frag", glGetShaderInfoLog(fsId).decode()))

    billboardProgram = glCreateProgram()
    glAttachShader(billboardProgram, vsId)
    glAttachShader(billboardProgram, fsId)
    glLinkProgram(billboardProgram)

    if not glGetProgramiv(billboardProgram, GL_LINK_STATUS):
        raise Exception("Link failed")

    glUseProgram(billboardProgram)

    global circleColorUniform
    global circleRadiusUniform
    global circleOutputTypeUniform
    circleColorUniform = glGetUniformLocation(billboardProgram, "color")
    circleRadiusUniform = glGetUniformLocation(billboardProgram, "radius")
    circleOutputTypeUniform = glGetUniformLocation(billboardProgram, "outputType")


def main():
    glutInit(sys.argv)
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH)
    glutInitWindowSize(size[0], size[1])
    glutCreateWindow(name)

    glClearColor(0., 0., 0., 1.)
    glutDisplayFunc(display)

    setupObjectballsShader()
    setupBillboardsShader()

    glutMainLoop()
    return


def saveBufferAs(filename):
    glReadBuffer(GL_FRONT)
    data = glReadPixels(0, 0, size[0], size[1], GL_RGB, GL_UNSIGNED_BYTE)
    image = Image.frombytes("RGB", size, data)
    image = image.transpose(Image.FLIP_TOP_BOTTOM)
    image.save(filename)

def drawFullscreenQuad():
    vertices = [
        -1, -1, 0,
        1, -1, 0,
        1, 1, 0,
        -1, 1, 0
    ]
    indices = [
        0, 1, 2, 2, 3, 0
    ]

    glBegin(GL_TRIANGLES)
    for i in range(0, len(indices), 3):
        index = indices[i] * 3
        glVertex3f(*vertices[index:index + 3])

        index = indices[i + 1] * 3
        glVertex3f(*vertices[index:index + 3])

        index = indices[i + 2] * 3
        glVertex3f(*vertices[index:index + 3])

    glEnd()

def drawBillboard(ball, color):
    s = 2 * ball[3]
    vertices = [
        - s/2, - s/2, 0.0,
        + s/2, - s/2, 0.0,
        + s/2, + s/2, 0.0,
        - s/2, + s/2, 0.0
    ]
    texcoords = [
        0, 0, 0,
        1, 0, 0,
        1, 1, 0,
        0, 1, 0
    ]
    indices = [
        0, 1, 2, 2, 3, 0
    ]

    global circleColorUniform
    global circleRadiusUniform
    glUniform3f(circleColorUniform, *color)
    glUniform1f(circleRadiusUniform, ball[3])

    glEnable(GL_TEXTURE_2D)
    glBegin(GL_TRIANGLES)
    glNormal3f(ball[0], ball[1], ball[2])
    for i in range(0, len(indices), 3):
        index = indices[i] * 3
        glTexCoord2f(*texcoords[index:index + 2])
        glVertex3f(*vertices[index:index + 3])

        index = indices[i + 1] * 3
        glTexCoord2f(*texcoords[index:index + 2])
        glVertex3f(*vertices[index:index + 3])

        index = indices[i + 2] * 3
        glTexCoord2f(*texcoords[index:index + 2])
        glVertex3f(*vertices[index:index + 3])

    glEnd()

def generateBalls():
    balls = []
    ballColors = []
    ballCount = int(random.uniform(7, 30))
    md = 6
    for x in range(0, ballCount):
        balls += [[random.uniform(-md, md), random.uniform(-md, md), random.uniform(8, 20), random.uniform(1, 1)]]
        ballColors += [[random.random(), random.random(), random.random()]]

    return ballCount, balls, ballColors

def display():

    debug = False
    if debug:
        ballCount = 2
        balls = [[5, 0, 20, 1], [0, 0, 8, 1]]
        ballColors = [[1, 1, 1], [1, 0, 0]]
        ballCount, balls, ballColors = generateBalls()
        drawBalls(ballCount, balls, ballColors, "out")
        return

#    for i in range(0, 10000):
    for i in range(10000, 20000):
        ballCount, balls, ballColors = generateBalls()
        outPath = "out2/" + str(i).zfill(5)
        print(outPath)
        drawBalls(ballCount, balls, ballColors, outPath)

    sys.exit(0)

def drawBalls(ballCount, balls, ballColors, outPath):
    if not os.path.exists(outPath):
        os.makedirs(outPath)

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

    global objectballsProgram
    glUseProgram(objectballsProgram)

    glUniform4fv(metaballsUniform, ballCount, balls)
    glUniform3fv(metaballColorsUniform, ballCount, ballColors)
    glUniform1i(ballsCountUniform, ballCount)

    glUniform1i(outputTypeUniform, 0)
    drawFullscreenQuad()
    glutSwapBuffers()
    saveBufferAs(outPath + "/color.png")

    glUniform1i(outputTypeUniform, 1)
    drawFullscreenQuad()
    glutSwapBuffers()
    saveBufferAs(outPath + "/depth.png")

    glUniform1i(outputTypeUniform, 2)
    drawFullscreenQuad()
    glutSwapBuffers()
    saveBufferAs(outPath + "/norm.png")

    glUniform1i(outputTypeUniform, 3)
    drawFullscreenQuad()
    glutSwapBuffers()
    saveBufferAs(outPath + "/shaded.png")

    glUniform1i(outputTypeUniform, 0)
    drawFullscreenQuad()
    glUseProgram(billboardProgram)

    glEnable(GL_DEPTH_TEST)
    glClearColor(0, 0, 0, 1)
    glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT)
    glDepthFunc(GL_LESS)
    global circleOutputTypeUniform
    glUniform1i(circleOutputTypeUniform, 0)
    for x in range(0, ballCount):
        drawBillboard(balls[x], ballColors[x])

    glutSwapBuffers()
    saveBufferAs(outPath + "/splat.png")

    glClearColor(1, 1, 1, 1)
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    glUniform1i(circleOutputTypeUniform, 1)
    for x in range(0, ballCount):
        drawBillboard(balls[x], ballColors[x])

    glutSwapBuffers()
    saveBufferAs(outPath + "/splatdepth.png")
    glDisable(GL_DEPTH_TEST)


if __name__ == '__main__': main()
