import os
import random
import sys

import glfw
import numpy as np
from OpenGL.GL import *
from PIL import Image

from generator.renderer_objectspace import ObjectMetaballsRenderer
from generator.renderer_screenspace import CircleMetaballsRenderer

dirname = os.path.dirname(__file__)
outDir = os.path.join(dirname, "out/generator/")

GENERATE_COUNT = 20000 # 200000
size = (256, 256)
window = -1

objectMetaballsRenderer = ObjectMetaballsRenderer()
circleMetaballsRenderer = CircleMetaballsRenderer()


def main():
    if not glfw.init():
        return

    global window
    window = glfw.create_window(size[0], size[1], "Generator", None, None)
    if not window:
        glfw.terminate()
        return

    glfw.make_context_current(window)
    glfw.swap_interval(1)

    glClearColor(0., 0., 0., 1.)

    objectMetaballsRenderer.setup()
    circleMetaballsRenderer.setup()

    while not glfw.window_should_close(window):
        display()
        glfw.poll_events()

    glfw.terminate()


def save_buffer_as(filename):
    glReadBuffer(GL_FRONT)
    data = glReadPixels(0, 0, size[0], size[1], GL_RGB, GL_UNSIGNED_BYTE)
    image = Image.frombytes("RGB", size, data)
    image = image.transpose(Image.FLIP_TOP_BOTTOM)
    image.save(filename)


def generate_balls():
    balls = []
    colors = []
    count = int(random.uniform(0, 35))
    xy_range = 6
    for _ in range(0, count):
        balls += [[random.uniform(-xy_range, xy_range), random.uniform(-xy_range, xy_range), random.uniform(8, 22), random.uniform(1, 1)]]
        colors += [[random.random(), random.random(), random.random()]]

    return {
        'count': count,
        'colors': colors,
        'balls': balls
    }

def permute(balls):
    centers = []

    for center in balls['balls']:
        center[0] += random.uniform(-100, 100) / 100.0
        center[1] += random.uniform(-100, 100) / 100.0
        center[2] += random.uniform(-100, 100) / 100.0
        center[2] = np.clip(center[2], 8, 22)
        centers.append(center)

    return {
        'count': balls['count'],
        'colors': balls['colors'],
        'balls': centers
    }


def display():
    permutations = 3
    for i in range(0, GENERATE_COUNT, permutations):
        balls = generate_balls()

        for p in range(0, permutations):
            out_path = os.path.join(outDir, str(i + p).zfill(6))
            print(out_path)

            balls = permute(balls)
            draw_balls(balls, out_path)

    sys.exit(0)


def draw_balls(balls, out_path):
    glfw.poll_events()
    os.makedirs(out_path, exist_ok=True)

    def swap_and_save(name):
        glfw.swap_buffers(window)
        save_buffer_as(out_path + "/" + name + ".png")

    objectMetaballsRenderer.render_color(balls)
    swap_and_save("obj_color")

    objectMetaballsRenderer.render_depth(balls)
    swap_and_save("obj_depth")

    objectMetaballsRenderer.render_normals(balls)
    swap_and_save("obj_norm")

    objectMetaballsRenderer.render_shaded(balls)
    swap_and_save("obj_shaded")

    circleMetaballsRenderer.render_color(balls)
    swap_and_save("circle_color")

    circleMetaballsRenderer.render_depth(balls)
    swap_and_save("circle_depth")

    glfw.poll_events()

if __name__ == '__main__':
    main()
