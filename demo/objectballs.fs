#version 120

#define METABALLS_COUNT 145

uniform vec4 metaballs[METABALLS_COUNT];
uniform vec3 metaballsColor[METABALLS_COUNT];
uniform int ballsCount;
uniform int outputType;

in vec2 uv;

#define FOV 30.
#define MIN_DEPTH 7.
#define MAX_DEPTH 25.
#define DEPTH_STEP 0.01
#define THRESHOLD 1.0

#define OUTPUT_TYPE_COLOR 0
#define OUTPUT_TYPE_DEPTH 1
#define OUTPUT_TYPE_NORMALS 2
#define OUTPUT_TYPE_SHADED 3


// field function
float ff(vec4 metaball, vec3 pos) {
    vec3 metaballCenter = metaball.xyz;
    float metaballSize = metaball.w;
    float dist = length(pos - metaballCenter);
    return 1 / pow(dist / metaballSize, 2.);
}

// field function normal, gradient of the field function
vec3 ffn(vec4 metaball, vec3 X) {
    vec3 C = metaball.xyz;
    float metaballSize = metaball.w;
    return 2*(C - X) / pow(length(C - X), 4) * pow(metaballSize, 2);
}

// Returns the a point dist along a ray shot through the uv plane at coordinate uv
vec3 ray(vec2 uv, float dist) {
    float camPlaneDist = 1.0;
    float fov = radians(FOV);
    vec2 centeredUv = (uv - 0.5) * 2;
    vec3 dir = normalize(vec3(centeredUv * camPlaneDist * tan(fov), camPlaneDist));
    vec3 pos = dist * dir;
    return pos;
}

// Shoots a ray through the uv plane and returns a vector of (rgb, depth) of metaballs hit
vec4 depthRay(vec2 uv) {
    for (float depth = MIN_DEPTH; depth < MAX_DEPTH; depth += DEPTH_STEP) {
        vec3 pos = ray(uv, depth);
        float fieldValue = 0.;
        vec3 color = vec3(0.);

        for (int i = 0; i < ballsCount; i++) {
            float dist = length(pos - metaballs[i].xyz);
            float ballField = ff(metaballs[i], pos);
            fieldValue += ballField;

            color += metaballsColor[i]*ballField;
        }

        if (fieldValue > THRESHOLD) {
            vec3 normColor = color / fieldValue;
            return vec4(normColor, depth);
            break;
        }
    }

    return vec4(0., 0., 0., 1000.);
}

// Shoots a ray through the uv plane and returns the normal vector of the metaballs hit
vec3 normRay(vec2 uv) {
    for (float depth = MIN_DEPTH; depth < MAX_DEPTH; depth += DEPTH_STEP) {
        vec3 pos = ray(uv, depth);
        float fieldValue = 0.;
        vec3 norm = vec3(0.);

        for (int i = 0; i < ballsCount; i++) {
            float dist = length(pos - metaballs[i].xyz);
            float ballField = ff(metaballs[i], pos);
            fieldValue += ballField;

            norm += ffn(metaballs[i], pos);
        }

        if (fieldValue > THRESHOLD) {
            return normalize(norm);
        }
    }

    return vec3(0., 0., 0.);
}

float blinnPhongSpecular(
    vec3 lightDirection,
    vec3 viewDirection,
    vec3 surfaceNormal,
    float shininess) {

    //Calculate Blinn-Phong power
    vec3 H = normalize(viewDirection + lightDirection);
    return pow(max(0.0, dot(surfaceNormal, H)), shininess);
}

void mainImage(out vec4 fragColor, in vec2 fragCoord) {
    fragColor = vec4(0.0, 0.0, 0.0, 1.0);

    if (outputType == OUTPUT_TYPE_COLOR) {
        vec4 depthRayValue = depthRay(uv);
        fragColor.rgb = depthRayValue.rgb;
    } else if (outputType == OUTPUT_TYPE_DEPTH) {
        vec4 depthRayValue = depthRay(uv);
        float depth = depthRayValue.w;
        fragColor.rgb = vec3((depth - MIN_DEPTH) / (MAX_DEPTH - MIN_DEPTH));
    } else if (outputType == OUTPUT_TYPE_NORMALS) {
        vec3 norm = normRay(uv);
        fragColor.rgb = (norm + 1.) * 0.5 * length(norm);
    } else if (outputType == OUTPUT_TYPE_SHADED) {
        // simple blinn phong shading
        vec3 lightPosition = vec3(3., 0., 0.);

        vec4 depthRayValue = depthRay(uv);
        vec3 surfaceNormal = normRay(uv);

        vec3 surfacePosition = ray(uv, depthRayValue.w);
        vec3 eyeDirection = normalize(-surfacePosition);
        vec3 lightDirection = normalize(lightPosition - surfacePosition);
        vec3 normal = normalize(surfaceNormal);

        float shininess = 10.;
        float power = blinnPhongSpecular(lightDirection, eyeDirection, -normal, shininess);
        float diff = dot(lightDirection, normal);

        fragColor.rgb = depthRayValue.rgb * max(diff, 0.5) + vec3(power);
    }
}

void main() {
    mainImage(gl_FragColor, uv);
}