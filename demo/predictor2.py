from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import cv2

from examples import load_examples_placeholder
from model import *


def start_session():
    a = parse_arguments()

    if a.seed is None:
        a.seed = random.randint(0, 2 ** 31 - 1)

    tf.set_random_seed(a.seed)
    np.random.seed(a.seed)
    random.seed(a.seed)

    if not os.path.exists(a.output_dir):
        os.makedirs(a.output_dir)

    if a.checkpoint is None:
        raise Exception("checkpoint required for prediction")

    # load some options from the checkpoint
    options = {"ngf", "ndf"}
    with open(os.path.join(a.checkpoint, "options.json")) as f:
        for key, val in json.loads(f.read()).items():
            if key in options:
                print("loaded", key, "=", val)
                setattr(a, key, val)

    # disable these features in predict mode
    a.scale_size = CROP_SIZE
    a.flip = False
    a.inferring = True

    for k, v in a._get_kwargs():
        print(k, "=", v)

    with open(os.path.join(a.output_dir, "options.json"), "w") as f:
        f.write(json.dumps(vars(a), sort_keys=True, indent=4))

    model, examples = create_model(a, load_examples_placeholder)

    if a.lab_colorization:
        outputs = augment(model.outputs)
    else:
        outputs = deprocess(model.outputs)

    scaled_outputs = tf.cast(tf.multiply(outputs, 255), tf.uint8)

    # reverse any processing on images so they can be written to disk or displayed to user
    # with tf.name_scope("convert_outputs"):
    #    converted_outputs = convert(outputs)

    with tf.name_scope("encode_images"):
        display_fetches = {
            # "outputs": outputs,
            "scaled_outputs": scaled_outputs,
        }

    saver = tf.train.Saver(max_to_keep=1)

    # to enable cpu mode:
    # with sv.managed_session(config=tf.ConfigProto(device_count={'GPU':0})) as sess:
    # with sv.managed_session() as sess:
    #sess = tf.Session(config=tf.ConfigProto(device_count={'GPU':0}))
    sess = tf.Session()

    def restore():
        if a.checkpoint is not None:
            print("loading model from checkpoint")
            checkpoint = tf.train.latest_checkpoint(a.checkpoint)
            saver.restore(sess, checkpoint)

    restore()
    input_placeholder = examples.input_placeholder

    return {
        "sess": sess,
        "input_placeholder": input_placeholder,
        "display_fetches": display_fetches,
        "restore": restore
    }


def run(pred, input):
    start = time.time()
    image = cv2.resize(input, dsize=(CROP_SIZE, CROP_SIZE), interpolation=cv2.INTER_CUBIC)

    start_run = time.time()
    results = pred['sess'].run(pred['display_fetches'], feed_dict={pred['input_placeholder']: image})
    end = time.time()
    #print("network run: " + str(end - start_run))

    result_image = results['scaled_outputs'][0, :, :, :]

    color_depth = result_image[:, :, 0:4]
    normals = result_image[:, :, 4:7]

    end = time.time()
    #print("do prediction: " + str(end - start))

#    return cv2.resize(result_image, dsize=input.shape[0:2], interpolation=cv2.INTER_CUBIC).astype('uint8')
    return color_depth, normals

