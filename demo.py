import math
import random

import numpy as np
import time
from OpenGL.GL import *
from OpenGL.GLUT import *
from PIL import Image

from demo import predictor2
from model import CROP_SIZE

PRINT_TIME = False

# Render modes
RENDER_MODE_CIRCLE_DEPTH = 0
RENDER_MODE_CIRCLE_COLOR = 1
RENDER_MODE_OBJ_DEPTH = 2
RENDER_MODE_OBJ_COLOR = 3
RENDER_MODE_OBJ_NORM = 4
RENDER_MODE_OBJ_SHADED = 5
RENDER_MODE_AI_DEPTH = 6
RENDER_MODE_AI_COLOR = 7
RENDER_MODE_AI_NORM = 8
RENDER_MODE_AI_SHADED = 9

RenderModesAI = [RENDER_MODE_AI_DEPTH, RENDER_MODE_AI_COLOR, RENDER_MODE_AI_NORM, RENDER_MODE_AI_SHADED]
RenderModesCircle = [RENDER_MODE_CIRCLE_DEPTH, RENDER_MODE_CIRCLE_COLOR]
RenderModesObj = [RENDER_MODE_OBJ_DEPTH, RENDER_MODE_OBJ_COLOR, RENDER_MODE_OBJ_NORM, RENDER_MODE_OBJ_SHADED]
RenderModes = [RENDER_MODE_CIRCLE_DEPTH, RENDER_MODE_CIRCLE_COLOR, RENDER_MODE_AI_DEPTH, RENDER_MODE_AI_NORM, RENDER_MODE_AI_COLOR, RENDER_MODE_AI_SHADED, RENDER_MODE_OBJ_DEPTH, RENDER_MODE_OBJ_NORM, RENDER_MODE_OBJ_COLOR, RENDER_MODE_OBJ_SHADED]
RenderModeNames = ["circle_depth", "circle_color", "ai_depth", "ai_norm", "ai_color", "ai_shaded", "obj_depth", "obj_norm", "obj_color", "obj_shaded"]
renderMode = RenderModes[0]


dirname = os.path.dirname(__file__)

name = b'Deep Metaballs Demo'
size = (CROP_SIZE, CROP_SIZE)
objectballsProgram = -1
metaballsUniform = -1
metaballColorsUniform = -1
ballsCountUniform = -1
outputTypeUniform = -1

billboardProgram = -1
circleColorUniform = -1
circleRadiusUniform = -1
circleOutputTypeUniform = -1

textureProgram = -1
textureSamplerUniform = -1
textureAlphaUniform = -1
texture = -1

deferredProgram = -1
deferredNormalSamplerUniform = -1
deferredDiffuseSamplerUniform = -1
deferredNormalTexture = -1
deferredDiffuseTexture = -1

tfsess = -1
mouse_x = -1
mouse_y = -1

images_out_dir = os.path.join(os.path.dirname(__file__), "out/demo/")
images_saved = len(list(filter(lambda x: os.path.isdir(os.path.join(images_out_dir, x)), os.listdir(images_out_dir))))

class BallData:
    count = 2
    centers = [[5, 0, 20, 1], [0, 0, 8, 1]]
    colors = [[1, 1, 1], [1, 0, 0]]


class Camera:
    position = [0, 0, 0, 0]
    rot_x = 0
    rot_y = 0


ball_data = BallData()
camera = Camera()

def saveBufferAs(filename):
    os.makedirs(os.path.dirname(filename), exist_ok=True)
    glReadBuffer(GL_FRONT)
    data = glReadPixels(0, 0, size[0], size[1], GL_RGB, GL_UNSIGNED_BYTE)
    image = Image.frombytes("RGB", size, data)
    image = image.transpose(Image.FLIP_TOP_BOTTOM)
    image.save(filename)


def apply_camera_transform(ball_data, camera):
    centers = []

    #rot_x_matrix = [[1, 0, 0], [0, np.math.cos(camera.rot_x), -np.math.sin(camera.rot_x)], [0, np.math.sin(camera.rot_x), np.math.cos(camera.rot_x)]]
    #rot_y_matrix = [[np.math.cos(camera.rot_y), 0, np.math.sin(camera.rot_y)], [0, 1, 0], [-np.math.sin(camera.rot_y), 0, np.math.cos(camera.rot_y)]]

    rot_x_matrix = [[1, 0, 0, 0], [0, np.math.cos(camera.rot_x), -np.math.sin(camera.rot_x), 0], [0, np.math.sin(camera.rot_x), np.math.cos(camera.rot_x), 0], [0,0,0,1]]
    rot_y_matrix = [[np.math.cos(camera.rot_y), 0, np.math.sin(camera.rot_y), 0], [0, 1, 0, 0], [-np.math.sin(camera.rot_y), 0, np.math.cos(camera.rot_y), 0], [0,0,0,1]]

    for center in ball_data.centers:
        center = np.add(center, camera.position)
        center = np.matmul(rot_y_matrix, center)
        center = np.matmul(rot_x_matrix, center)
        centers.append(center)

    transformed_ball_data = BallData()
    transformed_ball_data.centers = centers
    transformed_ball_data.colors = ball_data.colors
    transformed_ball_data.count = ball_data.count

    return transformed_ball_data


def setupObjectballsShader():
    global objectballsProgram
    vert = """
        attribute vec4 v_position;
        varying vec2 uv;

        void main() {
          gl_Position = v_position;
          uv = (v_position + 1.) * 0.5;
        }
        """

    vsId = glCreateShader(GL_VERTEX_SHADER)
    glShaderSource(vsId, vert)
    glCompileShader(vsId)
    if not glGetShaderiv(vsId, GL_COMPILE_STATUS):
        raise Exception('failed to compile shader "%s":\n%s' % ("vert", glGetShaderInfoLog(vsId).decode()))

    fragFile = open(os.path.join(dirname, 'demo/objectballs.fs'))
    frag = fragFile.read()
    fragFile.close()

    fsId = glCreateShader(GL_FRAGMENT_SHADER)
    glShaderSource(fsId, frag)
    glCompileShader(fsId)
    if not glGetShaderiv(fsId, GL_COMPILE_STATUS):
        raise Exception('failed to compile shader "%s":\n%s' % ("frag", glGetShaderInfoLog(fsId).decode()))

    objectballsProgram = glCreateProgram()
    glAttachShader(objectballsProgram, vsId)
    glAttachShader(objectballsProgram, fsId)
    glLinkProgram(objectballsProgram)

    if not glGetProgramiv(objectballsProgram, GL_LINK_STATUS):
        raise Exception("Link failed :(")

    glUseProgram(objectballsProgram)

    global metaballsUniform
    global metaballColorsUniform
    global ballsCountUniform
    global outputTypeUniform
    metaballsUniform = glGetUniformLocation(objectballsProgram, "metaballs")
    metaballColorsUniform = glGetUniformLocation(objectballsProgram, "metaballsColor")
    ballsCountUniform = glGetUniformLocation(objectballsProgram, "ballsCount")
    outputTypeUniform = glGetUniformLocation(objectballsProgram, "outputType")


def setupBillboardsShader():
    global billboardProgram
    vertFile = open(os.path.join(dirname, 'demo/circle.vs'))
    vert = vertFile.read()
    vertFile.close()

    vsId = glCreateShader(GL_VERTEX_SHADER)
    glShaderSource(vsId, vert)
    glCompileShader(vsId)
    if not glGetShaderiv(vsId, GL_COMPILE_STATUS):
        raise Exception('failed to compile shader "%s":\n%s' % ("vert", glGetShaderInfoLog(vsId).decode()))

    fragFile = open(os.path.join(dirname, 'demo/circle.fs'))
    frag = fragFile.read()
    fragFile.close()

    fsId = glCreateShader(GL_FRAGMENT_SHADER)
    glShaderSource(fsId, frag)
    glCompileShader(fsId)
    if not glGetShaderiv(fsId, GL_COMPILE_STATUS):
        raise Exception('failed to compile shader "%s":\n%s' % ("frag", glGetShaderInfoLog(fsId).decode()))

    billboardProgram = glCreateProgram()
    glAttachShader(billboardProgram, vsId)
    glAttachShader(billboardProgram, fsId)
    glLinkProgram(billboardProgram)

    if not glGetProgramiv(billboardProgram, GL_LINK_STATUS):
        raise Exception("Link failed :(")

    glUseProgram(billboardProgram)

    global circleColorUniform
    global circleRadiusUniform
    global circleOutputTypeUniform
    circleColorUniform = glGetUniformLocation(billboardProgram, "color")
    circleRadiusUniform = glGetUniformLocation(billboardProgram, "radius")
    circleOutputTypeUniform = glGetUniformLocation(billboardProgram, "outputType")


def setupTextureShader():
    global textureProgram
    vert = """
        attribute vec4 v_position;
        varying vec2 uv;

        void main() {
          gl_Position = v_position;
          uv = (v_position + 1.) * 0.5;
          uv.y = 1. - uv.y;
          gl_TexCoord[0] = gl_MultiTexCoord0;
        }
        """

    vsId = glCreateShader(GL_VERTEX_SHADER)
    glShaderSource(vsId, vert)
    glCompileShader(vsId)
    if not glGetShaderiv(vsId, GL_COMPILE_STATUS):
        raise Exception('failed to compile shader "%s":\n%s' % ("vert", glGetShaderInfoLog(vsId).decode()))

    frag = """
        uniform sampler2D texture_w;
        uniform int draw_alpha;
        in vec2 uv;
        void main() {
            if (draw_alpha == 0) {
                gl_FragColor.rgb = texture2D(texture_w, uv).rgb;
            } else {
                gl_FragColor.rgb = texture2D(texture_w, uv).a;
            }
        }
        """

    fsId = glCreateShader(GL_FRAGMENT_SHADER)
    glShaderSource(fsId, frag)
    glCompileShader(fsId)
    if not glGetShaderiv(fsId, GL_COMPILE_STATUS):
        raise Exception('failed to compile shader "%s":\n%s' % ("frag", glGetShaderInfoLog(fsId).decode()))

    textureProgram = glCreateProgram()
    glAttachShader(textureProgram, vsId)
    glAttachShader(textureProgram, fsId)
    glLinkProgram(textureProgram)

    if not glGetProgramiv(textureProgram, GL_LINK_STATUS):
        raise Exception("Link failed :(")

    glUseProgram(textureProgram)

    global texture
    texture = glGenTextures(1)

    global textureSamplerUniform
    textureSamplerUniform = glGetUniformLocation(textureProgram, "texture_w")

    global textureAlphaUniform
    textureAlphaUniform = glGetUniformLocation(textureProgram, "draw_alpha")



def setupDeferredShader():
    global deferredProgram
    vert = """
        attribute vec4 v_position;
        varying vec2 uv;

        void main() {
          gl_Position = v_position;
          uv = (v_position + 1.) * 0.5;
          uv.y = 1. - uv.y;
        }
        """

    vsId = glCreateShader(GL_VERTEX_SHADER)
    glShaderSource(vsId, vert)
    glCompileShader(vsId)
    if not glGetShaderiv(vsId, GL_COMPILE_STATUS):
        raise Exception('failed to compile shader "%s":\n%s' % ("vert", glGetShaderInfoLog(vsId).decode()))

    frag = """
        #define FOV 30.
        #define MIN_DEPTH 7.
        #define MAX_DEPTH 25.
        #define DEPTH_STEP 0.01
        #define THRESHOLD 1.0

        uniform sampler2D texture_normal;
        uniform sampler2D texture_diffuse;
        in vec2 uv;
        
        float blinnPhongSpecular(
            vec3 lightDirection,
            vec3 viewDirection,
            vec3 surfaceNormal,
            float shininess) {
        
            //Calculate Blinn-Phong power
            vec3 H = viewDirection + lightDirection;
            return pow(max(0.0, dot(surfaceNormal, H) / (length(H) * length(surfaceNormal))), shininess);
        }

        vec3 ray(vec2 uv, float dist) {
            float camPlaneDist = 1.0;
            float fov = radians(FOV);
            vec2 centeredUv = (uv - 0.5) * 2;
            vec3 dir = normalize(vec3(centeredUv * camPlaneDist * tan(fov), camPlaneDist));
            vec3 pos = dist * dir;
            return pos;
        }
        
        vec3 surfacePos(vec2 uv, float depth) {
            return ray(uv, depth);
        }

        void main() {
            vec4 color_depth = texture2D(texture_diffuse, uv);
            vec3 diffuse = color_depth.rgb;
            float depthTex = color_depth.a;
            float depth = depthTex * (MAX_DEPTH - MIN_DEPTH) + MIN_DEPTH;

            vec3 normal = normalize(texture2D(texture_normal, uv) * 2. - 1.);
            
            if (depthTex >= 0.98) {
              discard;
            }
            
            vec3 lightPosition = vec3(3., 0., 0.);
            vec3 surfacePosition = surfacePos(uv, depth);
            vec3 eyeDirection = normalize(-surfacePosition);
            vec3 lightDirection = normalize(lightPosition - surfacePosition);
    
            float shininess = 10.;
            float power = blinnPhongSpecular(lightDirection, eyeDirection, -normal, shininess);
            float diff = dot(lightDirection, normal);
    
            gl_FragColor.rgb = diffuse * max(diff, 0.5) + vec3(power);
        }
        """

    fsId = glCreateShader(GL_FRAGMENT_SHADER)
    glShaderSource(fsId, frag)
    glCompileShader(fsId)
    if not glGetShaderiv(fsId, GL_COMPILE_STATUS):
        raise Exception('failed to compile shader "%s":\n%s' % ("frag", glGetShaderInfoLog(fsId).decode()))

    deferredProgram = glCreateProgram()
    glAttachShader(deferredProgram, vsId)
    glAttachShader(deferredProgram, fsId)
    glLinkProgram(deferredProgram)

    if not glGetProgramiv(deferredProgram, GL_LINK_STATUS):
        raise Exception("Link failed :(")

    glUseProgram(deferredProgram)

    global deferredNormalTexture
    deferredNormalTexture = glGenTextures(1)

    global deferredDiffuseTexture
    deferredDiffuseTexture = glGenTextures(1)

    global deferredNormalSamplerUniform
    deferredNormalSamplerUniform = glGetUniformLocation(deferredProgram, "texture_normal")

    global deferredDiffuseSamplerUniform
    deferredDiffuseSamplerUniform = glGetUniformLocation(deferredProgram, "texture_diffuse")


def main():
    glutInit(sys.argv)
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH | GLUT_ALPHA)
    glutInitWindowSize(size[0], size[1])
    glutCreateWindow(name)

    glClearColor(0., 0., 0., 1.)
    glutDisplayFunc(display)
    glutMouseFunc(mouse)
    glutMotionFunc(motion)
    glutKeyboardFunc(keyboard)

    setupObjectballsShader()
    setupBillboardsShader()
    setupTextureShader()
    setupDeferredShader()

    global tfsess
    tfsess = predictor2.start_session()

    glutMainLoop()
    return


def drawFullscreenQuad():
    vertices = [
        -1, -1, 0,
        1, -1, 0,
        1, 1, 0,
        -1, 1, 0
    ]
    indices = [
        0, 1, 2, 2, 3, 0
    ]

    glBegin(GL_TRIANGLES)
    for i in range(0, len(indices), 3):
        index = indices[i] * 3
        glVertex3f(*vertices[index:index + 3])

        index = indices[i + 1] * 3
        glVertex3f(*vertices[index:index + 3])

        index = indices[i + 2] * 3
        glVertex3f(*vertices[index:index + 3])

    glEnd()

def drawBillboard(ball, color):
    s = 3 * ball[3]
    vertices = [
        - s/2, - s/2, 0.0,
        + s/2, - s/2, 0.0,
        + s/2, + s/2, 0.0,
        - s/2, + s/2, 0.0
    ]
    texcoords = [
        0, 0, 0,
        1, 0, 0,
        1, 1, 0,
        0, 1, 0
    ]
    indices = [
        0, 1, 2, 2, 3, 0
    ]

    global circleColorUniform
    global circleRadiusUniform
    glUniform3f(circleColorUniform, *color)
    glUniform1f(circleRadiusUniform, ball[3])

    glEnable(GL_TEXTURE_2D)
    glBegin(GL_TRIANGLES)
    glNormal3f(ball[0], ball[1], ball[2])
    for i in range(0, len(indices), 3):
        index = indices[i] * 3
        glTexCoord2f(*texcoords[index:index + 2])
        glVertex3f(*vertices[index:index + 3])

        index = indices[i + 1] * 3
        glTexCoord2f(*texcoords[index:index + 2])
        glVertex3f(*vertices[index:index + 3])

        index = indices[i + 2] * 3
        glTexCoord2f(*texcoords[index:index + 2])
        glVertex3f(*vertices[index:index + 3])

    glEnd()


def generate_balls():
    centers = []
    colors = []
    count = int(random.uniform(7, 30))
    md = 6
    for x in range(0, count):
        centers += [[random.uniform(-md, md), random.uniform(-md, md), random.uniform(8, 20), 1]]
        colors += [[random.random(), random.random(), random.random()]]

    data = BallData()
    data.centers = centers
    data.colors = colors
    data.count = count
    return data


def toggleRenderMode():
    global renderMode
    renderModeIndex = RenderModes.index(renderMode)
    newRenderModeIndex = (renderModeIndex + 1) % len(RenderModes)
    renderMode = RenderModes[newRenderModeIndex]

def mouse(button, state, x, y):
    global ball_data, mouse_x, mouse_y

    mouse_x = x
    mouse_y = y

    if (button == 2):
        ball_data = generate_balls()
        return

    if (button == 1):
        toggleRenderMode()


def keyboard(key, x, y):
    global camera
    global images_saved
    global renderMode

    if (key == b' '):
        toggleRenderMode()
    elif key == b'w':
        camera.position[2] -= 0.5
    elif key == b's':
        camera.position[2] += 0.5
    elif key == b'a':
        camera.position[0] += 0.5
    elif key == b'd':
        camera.position[0] -= 0.5
    elif key == b'r':
        camera.position = [0, 0, 0, 0]
        camera.rot_x = 0
        camera.rot_y = 0
    elif key == b'x':
        images_saved += 1
        for render_mode in range(0, len(RenderModes)):
            renderMode = RenderModes[render_mode]
            display()
            saveBufferAs(os.path.join(images_out_dir, str(images_saved), RenderModeNames[render_mode] + ".png"))
    elif key == b'c':
        images_saved += 1
        saveBufferAs("out/demo/demo" + str(images_saved) + ".png")
    elif 0 <= ord(key) - ord('0') <= len(RenderModes):
        index = ord(key) - ord('0') - 1
        if key == b'0':
            index = 9

        renderMode = RenderModes[index]

    glutPostRedisplay()


def motion(x, y):
    global ball_data, camera, mouse_x, mouse_y

    # wx = (x / glutGet(GLUT_WINDOW_WIDTH)) * 2 - 1
    # wy = (y / glutGet(GLUT_WINDOW_HEIGHT)) * 2 - 1
    # ball = ball_data.centers[0]
    # ball[0] = wx * 10
    # ball[1] = -wy * 10
    # ball_data.centers[0] = ball

    dx = x - mouse_x
    dy = y - mouse_y

    camera.rot_x -= dy / 150.0
    camera.rot_y -= dx / 150.0

    mouse_x = x
    mouse_y = y

    glutPostRedisplay()


def display():
    global ball_data, camera
    transformed_balls = apply_camera_transform(ball_data, camera)
    drawBalls(transformed_balls)


def drawBalls(ball_data):
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

    global renderMode
    global billboardProgram
    global objectballsProgram

    if renderMode in RenderModesObj:
        # object space method
        glUseProgram(objectballsProgram)

        glUniform4fv(metaballsUniform, ball_data.count, np.array(ball_data.centers).flatten())
        glUniform3fv(metaballColorsUniform, ball_data.count, ball_data.colors)
        glUniform1i(ballsCountUniform, ball_data.count)

        if renderMode == RENDER_MODE_OBJ_DEPTH:
            glUniform1i(outputTypeUniform, 1)
        elif renderMode == RENDER_MODE_OBJ_COLOR:
            glUniform1i(outputTypeUniform, 0)
        elif renderMode == RENDER_MODE_OBJ_NORM:
            glUniform1i(outputTypeUniform, 2)
        elif renderMode == RENDER_MODE_OBJ_SHADED:
            glUniform1i(outputTypeUniform, 3)

        drawFullscreenQuad()
        glutSwapBuffers()

    if renderMode in RenderModesCircle or renderMode in RenderModesAI:
        start = time.time()

        glUseProgram(billboardProgram)
        global circleOutputTypeUniform

        width = glutGet(GLUT_WINDOW_WIDTH)
        height = glutGet(GLUT_WINDOW_HEIGHT)

        if renderMode == RENDER_MODE_CIRCLE_COLOR:
            glEnable(GL_DEPTH_TEST)
            glClearColor(0, 0, 0, 1)
            glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT)
            glDepthFunc(GL_LESS)
            glUniform1i(circleOutputTypeUniform, 0)
            for x in range(0, ball_data.count):
                drawBillboard(ball_data.centers[x], ball_data.colors[x])

            glutSwapBuffers()

        elif renderMode == RENDER_MODE_CIRCLE_DEPTH:
            glClearColor(1, 1, 1, 1)
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
            glUniform1i(circleOutputTypeUniform, 1)
            for x in range(0, ball_data.count):
                drawBillboard(ball_data.centers[x], ball_data.colors[x])

            glutSwapBuffers()

        else:
            # neural network method
            # render circles
            start_draw_circles = time.time()
            glEnable(GL_DEPTH_TEST)
            glClearColor(0, 0, 0, 1)
            glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT)
            glDepthFunc(GL_LESS)
            glUniform1i(circleOutputTypeUniform, 0)
            for x in range(0, ball_data.count):
                drawBillboard(ball_data.centers[x], ball_data.colors[x])

            end_draw_circles = time.time()
            if PRINT_TIME:
                print("draw circles: " + str(end_draw_circles - start_draw_circles))

            start_read_back = time.time()
            glReadBuffer(GL_BACK)
            circle_data = glReadPixels(0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE)
            end = time.time()
            if PRINT_TIME:
                print("read circles 1: " + str(end - start_read_back))
            np_image = np.fromstring(circle_data, np.uint8).reshape(height, width, 4)
            np_image = np.flip(np_image, axis = 0)
            end = time.time()
            if PRINT_TIME:
                print("read circles: " + str(end - start_read_back))

            start_run_network = time.time()
            color_depth, normals = predictor2.run(tfsess, np_image)
            end_run_network = time.time()
            if PRINT_TIME:
                print("run network: " + str(end_run_network - start_run_network))

            start_after_network = time.time()
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

            if renderMode == RENDER_MODE_AI_SHADED:
                # deferred shading!
                global deferredProgram
                global deferredNormalTexture, deferredDiffuseTexture
                global deferredNormalSamplerUniform, deferredDiffuseSamplerUniform
                glUseProgram(deferredProgram)
                glEnable(GL_TEXTURE_2D)

                glActiveTexture(GL_TEXTURE1)
                glPixelStorei(GL_UNPACK_ALIGNMENT, 1)
                glBindTexture(GL_TEXTURE_2D, deferredNormalTexture)
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
                glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, normals.shape[0], normals.shape[1], 0, GL_RGB, GL_UNSIGNED_BYTE, normals)
                glUniform1i(deferredNormalSamplerUniform, 1)

                glActiveTexture(GL_TEXTURE2)
                glPixelStorei(GL_UNPACK_ALIGNMENT, 1)
                glBindTexture(GL_TEXTURE_2D, deferredDiffuseTexture)
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
                glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, color_depth.shape[0], color_depth.shape[1], 0, GL_RGBA, GL_UNSIGNED_BYTE, color_depth)
                glUniform1i(deferredDiffuseSamplerUniform, 2)

            else:
                global textureProgram
                global texture
                global textureSamplerUniform
                global textureAlphaUniform
                glUseProgram(textureProgram)
                glEnable(GL_TEXTURE_2D)
                glActiveTexture(GL_TEXTURE0)
                glPixelStorei(GL_UNPACK_ALIGNMENT, 1)
                glBindTexture(GL_TEXTURE_2D, texture)
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)

                if renderMode == RENDER_MODE_AI_DEPTH:
                    ai_image = color_depth
                    glUniform1i(textureAlphaUniform, 1)
                elif renderMode == RENDER_MODE_AI_COLOR:
                    ai_image = color_depth
                    glUniform1i(textureAlphaUniform, 0)
                elif renderMode == RENDER_MODE_AI_NORM:
                    ai_image = normals
                    glUniform1i(textureAlphaUniform, 0)


                if renderMode == RENDER_MODE_AI_NORM:
                    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, ai_image.shape[0], ai_image.shape[1], 0, GL_RGB,
                                 GL_UNSIGNED_BYTE, ai_image)
                else:
                    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, ai_image.shape[0], ai_image.shape[1], 0, GL_RGBA,
                                 GL_UNSIGNED_BYTE, ai_image)

                glUniform1i(textureSamplerUniform, 0)

            drawFullscreenQuad()
            glutSwapBuffers()
            end = time.time()
            
            if PRINT_TIME:
                print("assemble frame: " + str(end - start_after_network))
                print("draw frame: " + str(end - start))


if __name__ == '__main__': main()
